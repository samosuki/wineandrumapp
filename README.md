# Wine and Rum

Angular 2 app connecting to WordPress API to display articles from [Wine and Rum](http://www.wineandrum.com) website 

### Built With
* Ionic 2
* WordPress API

### To Do
* Add end to end testing
* Add push notifications

### Authors

Samora Reid [(samosuki)](https://bitbucket.org/samosuki/)

### Demo / App
Download - [Wine and Rum](https://play.google.com/store/apps/details?id=com.wineandrum.app)