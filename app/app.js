import 'es6-shim';
import {App, IonicApp, Platform, MenuController} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
import {CategoryPage} from './pages/category-list/categories';
import {AboutPage} from './pages/about/about';
import {ContactPage} from './pages/contact/contact';
import {ArticleListPage} from './pages/article-list/article';
import {ArticlePage} from './pages/article/article'
import {ArticleService} from './services/article-service';


@App({
  // menu to load first of all
  templateUrl: 'build/app.html',
  config: {}, // http://ionicframework.com/docs/v2/api/config/Config/
  providers: [ArticleService]
})
class MyApp {
  static get parameters() {
    return [[IonicApp], [Platform], [MenuController]];
  }

  constructor(app, platform, menu) {
    // set up our app
    this.app = app;
    this.platform = platform;
    this.menu = menu;
    this.initializeApp();

    // set our app's pages
    this.appPages = [
      { title: 'Categories', component: CategoryPage },
      { title: 'About', component: AboutPage }
      //{ title: 'Contact', component: ContactPage },
      //{ title: 'Article (Temp)', component: ArticleListPage }
    ];

    // make CategoryPage the root (or first) page
    this.rootPage = CategoryPage;
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    let nav = this.app.getComponent('nav');
    nav.setRoot(page.component);
  }
}
