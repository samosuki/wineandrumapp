describe('MyApp', function () {
    beforeEach(function () {
        browser.get('/');
    });
    it('should have the title - topics', function () {
        expect(browser.getTitle()).toEqual('TOPICS');
    });
    it('should have category - food', function () {
        var subject = element(by.id('cat_food')).getText();
        var result = "FOOD";
        expect(subject).toEqual(result);
    });
    it('should have category - places', function () {
        var subject = element(by.id('cat_places')).getText();
        var result = 'PLACES';
        expect(subject).toEqual(result);
    });
    it('should have category - austria', function () {
        var subject = element(by.id('cat_austria')).getText();
        var result = 'AUSTRIA';
        expect(subject).toEqual(result);
    });
    it('should have category - barabdos', function () {
        var subject = element(by.id('cat_barbados')).getText();
        var result = 'BARBADOS';
        expect(subject).toEqual(result);
    });
    it('should have 4 categories', function () {
        var subject = element.all(by.css('.advanced-background-title'));
        var result = 4;
        expect(subject.count()).toBe(result);
    });
});
