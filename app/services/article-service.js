import {Injectable} from 'angular2/core';
import {Http, Headers} from "angular2/http";
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
//import 'rxjs/add/operator/map';

let SERVER_URL = "http://localhost:8100/"

@Injectable()
export class ArticleService {

		static get parameters() {
				return [[Http]];
		}

		constructor (http) {
				this.http = http;
		}
		getAboutPage() {
				var url = 'http://wineandrum.com/wp-json/wp/v2/pages/11';
				this.response = this.http.get(url)
					.map(res => res.json())
					.catch(this.handleError);
					console.log(this.response);
				return this.response;
    }
		getAllPosts() {
        return this.http.get('http://wineandrum.com/wp-json/wp/v2/posts')
          .map(res => res.json())
					.catch(this.handleError);
					console.log(res);
					console.log(res.json);
    }
		getPostsbyCat(categoryName) {
			var url = 'http://wineandrum.com/wp-json/wp/v2/posts?filter[category_name]=' + categoryName + '&_embed';
			this.response = this.http.get(url)
				.map(res => res.json())
				.catch(this.handleError);
				console.log(this.response);
			return this.response;
    }
		getPostCountByCat(categoryName) {
			var url = 'http://wineandrum.com/wp-json/wp/v2/posts?filter[category_name]=' + categoryName + '&_embed';
			this.response = this.http.get(url)
				.map(res => res.json())
				.catch(this.handleError);
				console.log(this.response.length);
			return this.response.length;
		}
		getPostbyID(postID) {
			var url = 'http://wineandrum.com/wp-json/wp/v2/posts/' + postID + '&_embed';
			this.response = this.http.get(url)
				.map(res => res.json())
				.catch(this.handleError);
			return this.response;
    }
		getPostbyClick(article) {
			this.response = article;
			console.log(this.response);
			return this.response;
    }
		handleError(error) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
