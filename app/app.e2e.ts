describe('MyApp', () => {

  beforeEach(() => {
    browser.get('/');
  });

	// check for title of initial page
  it('should have the title - topics', () => {
    expect(browser.getTitle()).toEqual('TOPICS');
  });

	// check for category food
	it('should have category - food', () => {
    let subject = element(by.id('cat_food')).getText();
    let result  = "FOOD";
    expect(subject).toEqual(result);
  });

	// check for category places
	it('should have category - places', () => {
    let subject = element(by.id('cat_places')).getText();
    let result  = 'PLACES';
    expect(subject).toEqual(result);
  });

	// check for category austria
	it('should have category - austria', () => {
    let subject = element(by.id('cat_austria')).getText();
    let result  = 'AUSTRIA';
    expect(subject).toEqual(result);
  });

	// check for category barbados
	it('should have category - barabdos', () => {
    let subject = element(by.id('cat_barbados')).getText();
    let result  = 'BARBADOS';
    expect(subject).toEqual(result);
  });

	// check that 4 categories exist
	it('should have 4 categories', () => {
    let subject = element.all(by.css('.advanced-background-title'));
    let result  = 4;
    expect(subject.count()).toBe(result);
  });
});
