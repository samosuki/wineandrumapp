import {OnInit} from 'angular2/core';
import {Page, NavController, NavParams} from 'ionic-angular';
import {ArticleService} from '../../services/article-service';

@Page({
  templateUrl: 'build/pages/about/about.html',
  providers: [ArticleService]
})

export class AboutPage {

  static get parameters() {
    return [[ArticleService],[NavController]];
  }

  constructor(ArticleService, nav) {
      this.articleService = ArticleService;
      this.nav = nav;
  }
  ngOnInit() {
    this.articleService.getAboutPage().subscribe(
            data => {
              this.aboutcontent = data;
              console.log(this.aboutcontent);
            },
            error => console.log(error),
            () => console.log('getAboutPage (init) completed')

        );
  }
}
