import {OnInit} from 'angular2/core';
import {Page, NavController, NavParams} from 'ionic-angular';
import {ArticleService} from '../../services/article-service';

@Page({
  templateUrl: 'build/pages/article/article.html',
	providers: [ArticleService]
})

export class ArticlePage {

  static get parameters() {
        return [[ArticleService],[NavController],[NavParams]];
    }

    constructor(ArticleService, nav, navParams) {
        this.articleService = ArticleService;
        this.nav = nav;
        this.article = navParams.get('article');
        console.log("--this article--");
        console.log(this.article);
    }

  /*ngOnInit() {
    this.articles = article;
    console.log('getArticle (init) completed');
  }*/
  ngOnInit() {
    this.articles = this.articleService.getPostbyClick(this.article);
    console.log('getPostbyClick (init) completed');
  }


}
