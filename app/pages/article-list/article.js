import {OnInit} from 'angular2/core';
import {Page, NavController, Loading, NavParams} from 'ionic-angular';
import {ArticleService} from '../../services/article-service';
import {ArticlePage} from '../article/article'

@Page({
  templateUrl: 'build/pages/article-list/article.html',
	providers: [ArticleService]
})

export class ArticleListPage {

  static get parameters() {
        return [[ArticleService],[NavController],[NavParams]];
    }

    constructor(ArticleService, nav, navParams) {
        this.articleService = ArticleService;
        this.nav = nav;
        this.categoryName = navParams.get('categoryName');
    }

    presentLoadingText() {
      let loading = Loading.create({
        spinner: 'hide',
        content: 'Loading Please Wait...'
      });

      this.nav.present(loading);

      setTimeout(() => {
        this.nav.push(Page2);
      }, 1000);

      setTimeout(() => {
        loading.dismiss();
      }, 5000);
    }

    getPostsAll() {
      this.articleService.getAllPosts().subscribe(
              data => {
                this.articles = data.results;
                console.log(data);
              },
              error => console.log(error),
              () => console.log('getPosts completed')
          );
      }

      getPostsCat(categoryName) {
        this.articleService.getPostsbyCat(categoryName).subscribe(
                data => {
                  this.articles = data.results;
                  console.log(data);
                  console.log(data.length);
                },
                error => console.log(error),
                () => console.log('getPostsCat completed')
            );
        }

      gotoFullArticle(article) {
        //this.articleService
        this.nav.push(ArticlePage, {
          article: article
        });
        console.log(article);
      }

  ngOnInit() {
    this.articleService.getPostsbyCat(this.categoryName).subscribe(
            data => {
              this.articles = data;
              console.log(data);
              console.log(this.categoryName);
              console.log(this.articles);
              console.log(data.length);
            },
            error => console.log(error),
            () => console.log('getPostsCat (init) completed')
        );
  }


}
