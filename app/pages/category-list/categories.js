import {OnInit} from 'angular2/core';
import {Page, NavController, NavParams} from 'ionic-angular';
import {ArticleService} from '../../services/article-service';
import {ArticleListPage} from '../article-list/article';


@Page({
  templateUrl: 'build/pages/category-list/categories.html'
})
export class CategoryPage {

  static get parameters() {
    return [[NavController], [NavParams]];
  }
  constructor(nav, navParams) {
    this.nav = nav;
    this.categoryName = navParams.get('categoryName');
    console.log(this.categoryName);
  }

  gotoCategoryList(categoryName) {
     this.nav.push(ArticleListPage, {
       categoryName: categoryName
     });
     console.log(categoryName);
  }
}
